This section covers related work concerning penetration testing methodology and
currently available computer science testbeds. These two topics are divided into
two subsections respectively.

\subsection{Security Testing Guidelines and Standards}
\label{testingguides}
The conducted literature research resulted in one penetration testing standard
and five penetration testing guidelines.
The PCI Penetration Test Guidance is only an information supplement of
the PCI Data Security Standard and is not considered a standalone standard in this
paper.\\

The following standard and guidelines are being considered in the subsequent summaries:
\begin{itemize}
  \item OWASP Testing Guide v4 (OTGv4)
  \item Penetration Testing Execution Standard (PTES)
  \item PCI Penetration Test Guidance (PCI PTG)
  \item Open Source Security Testing Methodology Manual (OSSTMM)
  \item Information Systems Security Assessment Framework (ISSAF)
  \item NIST SP 800-115
\end{itemize}

The OTGv4 is focused on web application security. The guideline proposes to implement
testing in the software development lifecycle (SDLC) to reduce the effort of remediation
and testing itself. The testing model is described as a two-phase process,
consisting of a passive mode and an active mode.
The passive mode is described as a process in which the tester plays with the
application and understands its flow. The active mode begins with information gathering
and subsequent tests of defined security controls. In the OTGv4 the application tests are
described in great detail with their corresponding tools.
The OTGv4 does not describe tests concerning other components of a computer network nor
does it describe tests against humans.\cite{otgv4}\\

The PTES is a community project with the future intention of becoming a standard. Currently
the guideline is a work in progress, with several gaps in its test descriptions. As opposed to
the other guidelines the PTES establishes a very specific five-phase model of a
penetration test:
\begin{itemize}
  \item Intelligence Gathering
  \item Vulnerability Analysis
  \item Exploitation
  \item Post-Exploitation
  \item Reporting
\end{itemize}
Each Phase contains a detailed description of recommended tools and their usage. The PTES is
a good reference for networking, host and application attacks, but still needs
contribution for physical and human test cases.\cite{ptes2018}\\

The PCI Penetration Testing Guidance does mere represent a high-level overview
of the penetration testing process. The methodology is categorized into three phases:
Pre-Engagement, Engagement and Post-Engagement. The Pre-Engagement section includes
scoping and documentation of targets, the rules of engagement, like time windows of the assessment,
communication with partners and the handling of sensitive data (e.g. credit card holder information).
The engagement phase is then divided into three subsections, the application layer, the
network layer and the network segmentation without becoming too detailed in how
the tests should be conducted, but merely what a tester should look for.
The post-engagement contains suggestions for the remediation recommendations
and report structure. Furthermore, the guidance recommends to re-test implemented remediations.
The guidance does not propose a phase model of how to conduct a penetration test nor does it
showcase test cases for any OSI layer.\cite{pcidssguide2017}\\

The OSSTMM is a scientific paper on security assessments. It describes common test types,
establishes rules of engagement for the tester and then proposes two models, which
are used to build a Meta-Methodology, which fits every security assessment. The meta-methodology
is not divided into phases but into modules, which fulfill a single purpose in the
the process of checking the operational security status of the entity under test.
The guideline does provide tools to calculate the operational security but does
not state any supporting tools that aid in testing.\cite{osstmm2010}\\

The ISSAF is a comprehensive framework and focuses
on being a reference to security assessors and security administrators. It does not only
provide templates, checklists and best-practices for assessment management but also
supply the tester with a vast amount of test cases in the categories: network, host, application, and database. The described phase model of a penetration test consists of nine phases:
\begin{itemize}
  \item Information gathering
  \item Network Mapping
  \item Vulnerability identification
  \item Penetration
  \item Gaining access and escalate privileges
  \item Compromising remote users and sites
  \item Maintain access
  \item Covering tracks
\end{itemize}
Covering tracks is a new phase not covered by other guidelines. The ISSAF
does supplies its readers with recommendations about tools, attacks against every aspect
of a company and offers help in management with templates and checklists.\cite{issaf2006}\\

The NIST SP 800-115 standard is the only official standard know to the author, which
tackles penetration testing. It divides the assessment process into four
phases: planning, discovery, attack, and reporting. The planning phase discusses
assessment policies, selecting the targets and assessment techniques and developing
an assessment plan to define the boundaries of an assessment. The discovery phase is
subdivided into two parts: information gathering and vulnerability analysis. The difference
is the service or operating system detection (information gathering) and the lookup against a database, to uncover
vulnerable versions of a service or operating system (vulnerability analysis).
The attack phase is described as a process, which consists of gaining access,
escalate privileges, discover new information about the system by browsing and installing
additional tools to aid further scanning.
In the reporting phase, the standard discusses mitigation recommendations and suggests to
include the outcome of the root cause for each finding. It also refers to
the NIST SP 800-53, which recommends the alignment of mitigation recommendations with
security controls.
The NIST SP 800-115 is a high-level recommendation for security assessments without
touching on technical details and specific test cases.\cite{stouffer2008nist}\\

While the PTES is the most specific guideline the ISSAF is the most comprehensive one. The
PCI Penetration Testing Guidance and the NIST SP 800-115 can be used to become a
high-level overview of a penetration test. The OTGv4 supplies great detail
for web application centric penetration tests. The OSSTMM proposes a completely
new model for security assessments and its meta-methodology represents the most
versatile one. A compact summary of the guidelines can be seen in Table \ref{guidelinesummary}.

\begin{table*}[]
  \centering
\begin{tabular}{|l|c|c|c|c|c|c|c|c|}
\cline{4-8}
 \multicolumn{3}{c|}{} & \multicolumn{5}{c|}{Test case categories} \\ \cline{4-8}
\hline
 Name&  Tools&  Phase model&  Physical&  Network&  Host&  Application&  Human\\
 \hline
 OSSTMM&  \xmark&  \cmark&  \cmark&  \cmark&  \cmark&  \cmark&  \cmark\\
 \hline
 PTES&  \cmark&  \cmark&  \xmark&  \cmark&  \cmark&  \cmark&  \xmark\\
 \hline
 PCI PTG&  \xmark&  \xmark&  \xmark&  \xmark& \xmark&  \xmark&  \xmark\\
 \hline
 OTGv4&  \cmark&  \cmark&  \xmark&  \xmark&  \xmark&  \cmark&  \xmark\\
 \hline
 ISSAF&  \cmark&  \cmark&  \cmark&  \cmark&  \cmark&  \cmark&  \cmark\\
 \hline
 NIST SP 800-115&  \xmark&  \cmark&  \xmark&  \xmark&  \xmark&  \xmark&  \xmark\\ \hline
\end{tabular}
\caption{Summary of standard and guideline capabilities}
\label{guidelinesummary}
\end{table*}

\subsection{Currently Available Testbeds}
\label{testbeds}
There are plenty of computer science testbeds available, but the authors research only determined
one, that focuses on security research. This security research centric testbed facility is
called DETERLab.\\

DETERLab currently supports multiple concurrent experiments on the same physical substrate.
This is made possible by using different container virtualization technologies. Boiled down
to a basic principle, DETERLab can be understood as a cluster providing raw computing power.
Computing power can be shared with the DETERLab Federation Architecture among multiple facilities.
What makes DETERLab such a unique testbed, are their special capabilities in experiment handling
and human behavior simulation. They supply an orchestration
software, called MAGI, which supports automation from provisioning of the container to installing and
configuring software.
Large complex experiments, which need a lot of resources, might also benefit from
DETERLab multi-party experiment feature. A Multi-party experiment can serve different views
of the same experiment, depending on the researching topic of each participant. Another use case
of this feature can be Attack/Defense CTFs, to provide attackers and a defenders perspective of
the same computer network. Human behavior simulation can be done with DASH, an agent that simulates
modeled human behavior like the incorrect perception of security and stress.
DETERLab has overcome the challenges stated in section ~\ref{sectintro}, by using
container virtualization for isolation and performance and provide repeatability through MAGI. \cite{geniprojectdeterlabs}\\

Looking into EmuLab, the predecessor DETERLabs, the focus lies in the bare-metal allocation
and a network-centric setup. The reason for the bare-metal allocation is, that EmuLab wants
to minimize the interference between concurrent experiments. They are not achieving full isolation though, because
EmuLab uses network-mounted storage solutions, that are shared between users. Performance is
not a problem in EmuLab because deployed experiments do not compete against each other by using dedicated nodes.
Repeatability is achieved by developing experiments in Network simulator files, which are used
to set up experiments. Using the Network Simulator files, experiments can be ``swapped in'' and ``swapped out''
as needed.\cite{geniprojectemulab}\\

ORBIT is a testbed, which provides a radio grid emulator to conduct large-scale radio experiments and supports
several radio frequency protocols. The testbed does also have a real outdoor
wireless network to increase realism if researchers desire. It comprises of
nodes with multiple radio cards and software-defined radio FPGAs. To achieve isolation, ORBIT
makes use of sandboxes (small experiments) and OpenFlow (VLAN separation). Experiments
are described in Network Simulator files, like EmuLab. Performance can be controlled
via a noise injection framework.\cite{geniprojectorbit}\\

GENI is based on EmuLab, DETERLab, and ORBIT. Opposed to the central administration approach of federations
like in DETERLab and EmuLab, GENI focuses on the decentralized administration. Each campus
is administrating its own infrastructure. This infrastructure can consist of heterogeneous commercial-off-the-self
hardware and software and gets allocated through an Aggregate Manager.\cite{geniprojectarchitecture}\\
To isolate experiments GENI uses container virtualization and VLANs. Container virtualization
was chosen, because of its better performance towards virtual machines.\cite{geniprojectgee} Repeatability of the GENI testbeds is achieved by using OEDL, which
describes the resources and their configuration\cite{geniprojectoedl}.\\

Another testbeds solution, called FITS, is providing infrastructure for future network research.
Its distinguishing feature is network virtualization via two virtualization technologies, XEN, and OpenFlow.
This way the testbed is able to provide appropriate isolation depending on the research project.
To minimize the loss of packet forwarding performance with full-virtualization, the testbeds uses XEN. The paper
on the testbed does not describe any repeatability supporting technology.\cite{moraes2014fits}\\

The enumerated testbeds in academia are made for large-scale experiments and pursue
to provide computing power and scientific fidelity to some degree.
Interestingly enough, none of the enumerated testbeds focused on repeatability of conducted
experiments. ~\cite{ricci2015apt} proposes such a platform by using GENI's RSpec specification\cite{rspec2018geni}
to generate profiles, which can be shared
among researchers. This enables researchers to deploy the same experiment, without manually re-creating
the experiment's environment.\\

Besides the academic proposals and projects of testbeds, there exist also ``naive'' approaches
to building a security-focused testbed.\\

\cite{acosta2015scalable} proposes a testbed for penetration testing using virtualization via
XEN and the Common Open Research Emulator (CORE)\cite{core}. The CORE instance was virtualized on the XEN server
to support a scalable way, of added multiple virtual interfaces and being able to connect
physical devices to the CORE network. The paper points out issues, with the isolation of broadcast traffic
and additional physical hardware. The repeatability and performance of such a testbed
are not described in the paper. The paper states, that it solved isolation problems with
broadcast traffic by utilizing "host-only" networks.\\

CyRIS is a more advanced solution, that supports automation and was developed to build, a
so-called cyber range; a network scenario to train offensive and defensive skills in.
It instantiates complex network scenarios on manually selected KVM hosts
by creating virtual machines from predefined images. Furthermore, the CyRIS software
installs tools and simulates attacks. This solution enables its
users to re-create experiments, in this case, network scenarios. The isolation is not
in the focus of this solution since it does not assume a shared infrastructure.
The performance of CyRIS does only matter for provisioning time.\cite{pham2016cyris}\\

Another solution proposed by ~\cite{trickel2017shell} involves moving the hosted
infrastructure to an Infrastructure-as-a-Service provider like Amazon AWS. This solution
proposed a simple service, that is capable of setup Attack/Defense CTF infrastructure
as needed in an automated fashion. To achieve isolation Amazon AWS provides virtual private
clouds, which the paper used to isolate competing groups from each other. The proposed
service supplies its users with a library of vulnerable service to chose from. This cloud
makes repeatability of an experiment possible. In the paper, it is stated, that the authors
had several problems with performance, especially if the provisioned large game setups.
The root of those problems, were Amazon AWS itself, as it could not instantiate resources
as fast as they wanted and bottlenecks on the networking layer during DDoS attacks.\cite{trickel2017shell}\\

The simplest approach, which can be found is using Cisco Packet Tracer and GNS3 to
build testbeds\cite{montero2017design} on dedicated nodes similar to ~\cite{cardwell2016building} proposal.
Performance is limited by the resources of the dedicated node and repeatability is not achievable
without great effort. The isolation is given by the fact, that one machine is used for
a single experiment, quite like EmuLab's idea.
