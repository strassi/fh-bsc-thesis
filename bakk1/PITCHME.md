# Anforderungen an automatisierte Pentest Testumgebungen
---
## Worum geht es?
 - Analyse von unterstützenden Technologien         |
 - Schwachstellen / Angriffsvektoren                |
 - Analyse von etablierten Methodologien            |

Note:
 Virtualisierungstechnologien (Full, Container, SDN)--
 Methodologien für stanards und qualität
---
## Erkenntnisse
 - Isolation, Wiederholbarkeit und Performance      |
 - Testumgebungen mit Sicherheitsfokus              |
 - Testumgebungen mit Netzwerkfokus                 |
 - Testumgebungen ohne Fokus                        |
 - Guidelines und Standards                         |

Note:
 DETERLab Container Virt., große netwerke, Menschliches Verhalten
 nachahmbar, Experiment orchestrierung, Multi-party experiment --
 CyRIS Cyber range, ebenfalls orchestrierung, manuelles resourcen allokieren --
 GENI,EmuLab,ORBIT: Netzwerk, container, bare-metal, wireless --
 FITS XEN und OpenFlow --
 GENI - Global Environment for Network Innovations --
 OSSTMM, PTES, OWASP TGv4, NIST SP 800-115, PCI Guidance
---
## Was ist offen?
 - Unterstützende Software?                          |
  + Unterstützte Angriffsvektoren und Schwachstellen?|
  + Unterstützung etablierter Methodologien?         |

Note:
 COTS Open-Stack, Cloudstack, KVM, XEN, VMware, Docker, LXC --
 Attacks Software, Hardware (Rowhammer, Meltdown&Spectre), Social Engineering
 Bad USB --
 Method ausführung der Phasen, white-, grey- und black-box tests
---
## Fragen?
