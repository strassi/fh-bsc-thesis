# Bakk.Arbeit Teil 2

## Besserer Titel
 - Comparison of cloud solutions and their applicability for security research
 - Applicability of cloud solutions as security testbeds
 - Comparative analysis of cloud solutions for security testbeds
 
## Forschungsfragen und Resultat der Bakk2
Aus der Bakk1 sind folgende Fragen hervorgegangen:

- Welche Testumgebunen unterstützen praktizierte Penetrationtest Methodologien?
- Welche existierenden Open-Source Werkzeuge können für den Aufbau von Testumgebungen verwendet werden?
- Welche Schwachstellen werden von welcher Testumgebung unterstüzt?

Grundsätzlich ist das Resultat der Bakk2 eine Matrix, welche die Beziehung von Testumgebungen zu Schwachstellen und Methodologien
darstellt.

## Anregungen für die Literatursuche
Testumgebungen sind drei großen Herausfoderungen gegenüber gestellt:

- Wiederholbarkeit
- Isolation
- Performance

Aus diesen Gründen wird Literatur zu den Themen

- Automatisierung von Infrastruktur
- Configuration Management
- Schwachstellen/Attacken Klassifizierung
- Allokierung von Resourcen in verteilten Infrastrukturen
- Virtualisierungstechnologien (Container, Full, Process ...)
- Software-Defined-Networking
- Automatisches Provisionieren von Maschinen

wichtig sein.

## Anregungen für Testcases

Um Testumgebungen auf Schwachstellen-Unterstützung, Wiederholbarkeit, Isolation und Performance zu
testen werden,

- Proof-of-Concept Exploits
- Testcases
- Zeitmessungen

benötigt. Die Performance ist wohl das schwierigste zu messen, da es viele Performance-Faktoren
gibt.

Eine andere Möglichkeit wäre es sich nur auf Schwachstellen-Unterstützung zu konzentrieren und
weitere Bereiche für die Zukunft offen zu lassen.

Ein Ablauf eines Testcases für Testumgebungen könnte wie folgt ablaufen:

+ Node Deployment mit PXE
+ Testumgebung Infrastruktur aufbauen
+ Zeit Messung für Infrastruktur Deployment
+ Experiment Umgebung aufbauen
+ Zeit Messung für Experiment Deployment
+ Test-Attacken durchführen
+ Attacken Erfolg auswerten
+ Experiment Umgebung zerstören
+ Experiment-Ablauf wiederholen
+ Testumgebung Infrastruktur zerstören

Es wird davon ausgegangen, dass ein zentraler Management Node mit den nötigen Infrastrukturdiensten bereits besteht.

### Schwachstellentests

Die Testumgebungen können wie bereits beschrieben auf Schwachstellen-Unterstützung
getestet werden.
Mögliche Schwachstellen/Angriffsvektoren:

 - Hardware spezifische (z.B. Rowhammer, Meltdown&Spectre)
 - Betriebssystem-Schwachstellen (z.B. Mobile, Desktop, Embedded)
 - Netzwerk-Schwachstellen (z.B. ARP Poisoning, Switch->Hub Fallback)
 - Social Engineering (z.B. Phishing)
 - Schwachstellen Desktop-Applikationen (z.B. Office, Serverdienste)
 - Schwachstellen Web-Applikationen (z.B. \*-Injections, XSS)
 - Schwachsellen Mobile-Applikationen
 - Schwachstellen bei Virtualisierung (z.B. VM/container escapes)
