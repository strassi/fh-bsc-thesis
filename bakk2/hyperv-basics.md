# How Hyper-V works (on Windows Server 2016) [source](https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/hyper-v-technology-overview)
Is a role service for x64 Systems for Windows Server and Windows, but can also be downloaded as a [standalone product](https://technet.microsoft.com/library/hh923062.aspx). 
The required parts :

  - Windows hypervisor  
  - Hyper-V Virtual Machine Management Service  
  - virtualization WMI provider  
  - virtual machine bus (VMbus)   
  - virtualization service provider (VSP)  
  - virtual infrastructure driver (VID)
  
Supported OS: [source](https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/supported-linux-and-freebsd-virtual-machines-for-hyper-v-on-windows)

 - Linux
 - FreeBSD
 - Windows
 
For Linux and FreeBSD Hyper-V can provide emulated devices and Hyper-V-specific devices. Hyper-V-specific devices need drivers to be installed on Linux (LIS) and FreeBSD (BIS). This
may restrict older Kernel Versions of Linux and FreeBSD of being used as a guest operating system with Hyper-V-specific devices. According to the documentation with emulated
devices there should be no compatibility issues.


## What is a virtual machine? [source](https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/plan/should-i-create-a-generation-1-or-2-virtual-machine-in-hyper-v)
Hyper-V supports two types of virtual machines:

  - Generation 1
  - Generation 2
  
The difference between those two types are:

  - Generation 2 supports lager boot volumes and SecureBoot
  - Generation 2 supports less boot options
  - Generation 2 supports less Linux/BSD version
  - GEneration 2 supports only Windows 8 and above 64 bit

## Which hardware can be virtualized?

## How does network-virtualization work? [source](https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v-virtual-switch/hyper-v-virtual-switch)
To connect virtual machines with networks outside the Hyper-V networks, Microsoft provides software called the Virtual Switch. This is a software-based layer 2 switching
component which is able to be programmed to build SDNs.

The features of the Virtual Switch include:

  - ARP spoofing/poisoning protection
  - DHCP guard
  - VLANs
  - Port ACLs
  

## How does CPU-virtualization work?

## How does Memory-virtualization work?

## How does Storage-virtualization work?