# Virtual Machine Monitors under test

  - [VMware vSphere](https://www.vmware.com/support/pubs/vsphere-esxi-vcenter-server-pubs.html) - full-virtualization
  - [Microsoft Hyper-V](https://docs.microsoft.com/en-us/virtualization/index) - full-virtualization
  - [KVM](https://www.linux-kvm.org/page/Documents) - full-virtualization
  - [Xen Project](https://wiki.xenproject.org/wiki/Main_Page) - para-virtualization
  - [Docker](https://docs.docker.com/) - container-based OS virtualization
  - [LXC](https://linuxcontainers.org/lxc/documentation/) - container-based OS virtualization
  - [VServer](http://linux-vserver.org/Paper) - para-virtualization
  - Baremetal (No virtualization)

Reason: These software products provide full-virtualization, para-virtualization
container-based operating system virtualization and the baremetal is used
to test vulnerabilities without any virtualization overhead.

  - [VMware ThinApp](https://www.vmware.com/products/thinapp.html) - application virtualization
  - [Microsoft App-V](https://docs.microsoft.com/en-us/microsoft-desktop-optimization-pack/appv-v4/about-microsoft-application-virtualization-45) - application virtualization
