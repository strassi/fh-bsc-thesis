# How KVM (Kernel Virtual Machine) works
Two kernel modules are loaded `svm/vmx` and `kvm`. These create a `/dev/kvm` device in the userspace for interaction.
The `/dev/kvm` device is only controllable via ioctl() and creation of new VM returns a file descriptor. [SOURCE](https://www.linux-kvm.org/page/Initialization)

The software consists of the quemu emulator and kvm.

## What defines a Virtual Machine?

## Hardware available to the guest? 
Using Intel VT-d or AMD-I/O IOMMUs devices of the host can directly be made available to the guest. [SOURCE](https://www.linux-kvm.org/page/How_to_assign_devices_with_VT-d_in_KVM)

## How does networking works? [SOURCE](https://www.linux-kvm.org/page/Networking)
According to the kvm docs, networking can be done same as in quemu.
KVM provides five different network setups:

  - User Networking
  - Private Virtual Bridge
  - Public Bridge
  - iptables routing
  - VDE
  
User Networking is the slowest and provides access from the guest to the host, internet or local network resources. It does not provide access to the guest from outside.

A private bridge can be setup with the `brutils` and is used to connect 2 or more VMs in an isolated network. This network cannot be seen by other VMs nor other outside networks.

A public bridge connects the guest with the local network and makes it accessible. The public bridge also supports VLANs, but needs a para-virtualized `virtio` device in the guest to do so.

Using a `tap`-device a virtual machine can also be accessed via iptables routing mechanisms. The hypervisor is able to act as a firewall for the guest system.

VDE (virtual distributed ethernet) is not described in the documenation.

## How does CPU-Virtualization work? [Source](https://s3.amazonaws.com/academia.edu.documents/45826723/Recommendations_for_Virtualization_Techn20160521-17927-7687ic.pdf?AWSAccessKeyId=AKIAIWOWYYGZ2Y53UL3A&Expires=1523441720&Signature=WcxbJVxJioIJ6NlFlzd3tyDnnBc%3D&response-content-disposition=inline%3B%20filename%3DRecommendations_for_Virtualization_Techn.pdf)

KVM utilizes hardware-based CPU virtualization, thus needs the Intel VT-x oder AMDV CPU extensions.

## How does Memory-Virtualization work? [Source from 2010](https://www.linux-kvm.org/page/Memory)
quemu/kvm process allocates memory via malloc() like a normal process.

The quemu/kvm get mmu_notifiers to get information about the hosts intentions, when memory is swapped, which could concern a guest's memory.
If guest memory is involved, the guest gets the change to remove the page from the "shadow table" or EPT/NPT structures (Hardware structures). After that the host
is free to do whatever he wants to do with those pages.

The Intel/AMD virtualization extensions are used to trap changes in the register of the base tables and handle the guests intentions of 
changing memory.