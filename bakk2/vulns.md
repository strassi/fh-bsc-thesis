# Vulnerabilities under test
  - Hardware-based (Virtualization vs baremetal)
    - Meltdown/Spectre
    - RowHammer
  - Kernel Exploits  (for Container vs. trad. Virtualization)
  - Layer 2 Attacks (for Virtualization vs Baremetal)
    - ARP Flooding
    - ARP Spoofing
  - TCP Attacks (for full-virt vs container)
    - SYN Flooding
  - VM/Container Escapes (testing support for nested Virtualization)
    - VENOM (machbarkeit??)
  - ~Physical Attacks~
    - ~BADUSB~
    - ~ColdBoot~

# Guideline Anforderungen

  - Pentesting Phases (Pentesting Guideline  and ISSAF)
    - Information Gathering (false positives through virtualization)
    - Exploitation (Tests from above)
  - [Mitre CyberSecurity Projects](https://www.mitre.org/capabilities/cybersecurity/overview/cybersecurity-resources)
  - [ATTACK Mitre](https://attack.mitre.org/wiki/Main_Page)
  - [CAPEC Mitre](http://capec.mitre.org/data/definitions/3000.html) very interesting for classification
  - [Mitre OpenCourseWare](http://opensecuritytraining.info/Training.html)
