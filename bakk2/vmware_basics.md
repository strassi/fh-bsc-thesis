# How VMware virtualization works (vSphere 6.5)
This document describes the VMware vSphere 6.5 Product used as a target VMM for the bsc-thesis tests.

## What defines a Virtual Machine?
A virtual machine is a set of specification and configuration files.

Interesting and important files:

  - .vmx - Virtual Machine configuration file
  - .vmdk - Virtual Machine Disk characteristiscs
  - .nvram/nvram - BIOS or EFI configuration
  - .log - Virtual Machine log file
 
[Source](https://docs.vmware.com/en/VMware-vSphere/6.5/com.vmware.vsphere.vm_admin.doc/GUID-CEFF6D89-8C19-4143-8C26-4B6D6734D2CB.html)

## What hardware is available to Virtual Machines? [Source](https://docs.vmware.com/en/VMware-vSphere/6.5/com.vmware.vsphere.vm_admin.doc/GUID-80A23811-897E-457B-A05A-65E59E174427.html)
This depends on the VMM Host's hardware. For more have a look at the [compatability guide](http://www.vmware.com/resources/compatibility)

CPUs can be shared unmanipulated or manipulated by turning on/off features (Change identification mask).

Each virtual machine has a Virtual Machine Communication Interface device to the VMM (VMCI). This allows highspeed communication with the host and cannot be turned off.

## How does networking work? [source](https://docs.vmware.com/en/VMware-vSphere/6.5/com.vmware.vsphere.networking.doc/GUID-2B11DBB8-CB3C-4AFF-8885-EFEA0FC562F4.html)
Virtual Machines can be connected logically with eachother through virtual networks.

vSphere Standard Switches are represent a standard physical switch with reduced capabilities. They can be used to connect virtual machines within a virtual network or connect a physical network
with a virtual network by using uplink adapters (physical network interfaces on the VMM host).

VMkernel TCP/IP Networking Layer provides infrastructure services connectivity (vMotion, provisioning, vSAN etc.) and connectivity to their hosts.

Network I/O can be shaped using the vSphere Traffic Control Version 2/3.

## How does CPU-Virtualization work?[source](https://docs.vmware.com/en/VMware-vSphere/6.5/com.vmware.vsphere.resmgmt.doc/GUID-DFFA3A31-9EDD-4FD6-B65C-86E18644373E.html)
VMware distinguished between  
  - Software-based CPU-Virtualization  
  - Hardware-based CPU-Virtualization  

Software-based CPU-virtualzation runs guest application code directly on the CPU, while privileged code (page table updates, traps, system calls) get translated and then executed by the CPU.

HArdware-based CPU-virtualization must be supported by the underlying VMM CPU. This allows the guest to execute code directly on the CPU via a socalled "guest-mode". Privileged code needs a switch into "root mode"
and also gets executed directly on the CPU. The hypervisor handles the switches between "guest mode" and "root mode". Depening on the number of switches this type of 
virtualization can speed up execution of code.

## How does Memory-virtualization work? [Source](https://docs.vmware.com/en/VMware-vSphere/6.5/com.vmware.vsphere.resmgmt.doc/GUID-9D2D0E45-D741-476F-8DB1-F737839C2108.html)
Memory is handled by the VMKernel. It reserves some memory for its own purpose and devides the remaining memory into pages or 4KB to 2MB (depending). The VMM hosts
a mapping for "machine"-pages (hosts memory) to "physical"-pages (guests memory).  

VMware devides the types of memory virtualization into two categories:  
  - Software-based Memory-virtualization
  - Hardware-based Memory-virtualization
  
Software-based memory-virtualization is done by creating a shadow-paging table of the guests virtual to guests machine addresses in software and managed by the VMM.

Hardware-based memory-virtualization combines the guests mappings with the nested page tables of the VMM. It conists of two layers: 
  1. guests virtual-to-physical  
  2. guests physical-to-machine  
  




A virtual machine has three types of boundaries for memory allocation: 

  - Shares - Memory allocation if more than reservation is available
  - Reservation - Minimal memory allocation that is guaranteed in case of memory of host is depleted
  - Limit - Upper bound of physical memory allocation for the virtual machine
