# How to test

## Testing process
Based on PTES

The following phases:  
  - Information Gathering (check for false positives)  
  - Threat Modeling (already done by using predefined vulnerabilities)  
  - Vulnerability Analysis (already predefined with setup)  
  - Exploitation (execute exploit code)  
  - Post-Exploitation (Steal predefined information)  
  - Reports (Worked/Not Worked)  
  
The process is automated and using the following tools:  
  - Metasploit  
  - Nmap  
  - Custom PoCs  

## What is tested?
Capabilities of virtualization software in concern of it's support for different vulnerabilities.

We test vulnerabilities on four targets (AVOIDIT Paper):  
  - physical level  
  - network level  
  - operating system level  
  - application level  
  - user level  

This should enable the author to gain knowledge about the network interaction with virtualized Systems, discover differences about system virtualization techniques (container vs full),
check for anomlies in application level attacks and check for possibilities for user targeted exploits.

## What is not tested?
  - Performance of virtualized testbeds  
  - Isolation of virtualized testbeds  