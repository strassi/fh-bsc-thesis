# Bakk.-Arbeit Fortschritt
---
## Agenda
 - Planung
 - Review
---
## Planung
 - Inhaltsverzeichnis
 - Zeitplanung
---
## Inhaltsverzeichnis
 - Intro
 - Hintergrundbeschreibung (Was?, Wie?)
 - Metriken und Ergebnisse (Was kam raus?)
 - Zusammenfassung und weitere Forschungsmöglichkeiten

Note:
 1. Was mach ich und warum mach ich es?
 2. Was teste ich wie? (Verwendete Technologien, Testmethoden)
 3. Was messe ich und was kam raus? (Metriken)
 4. Was habe ich gemacht -> was kam raus -> was könnte man noch machen?
---
## Zeitplanung
 - **März** - Technologieauswahl und Testmethoden
 - **April** - Tests durchführen / Entscheidung für Prüfungstermin
 - **Mai Beginn** - Ergebnisse auswerten

Note:
  Wenig Puffer --
  April als Enscheidungsmonat für Prüfung
---
## Review
 - Paper Name: *AVOIDIT: A Cyber Attack Taxonomy?*
 - Chris Simmons/ University of Memphis
 - Paper von der ASIA (Annual Symposium on informational Assurance) 2014

Note:
  Warum das Paper: bietet einfache Klassifizierung von angriffen,
  soll mir bei auswahl der zu testenden angriffe helfen.
  Taxonomy/Klassifizierungsmethode + Prozess zur Benutzung
  KLassen: attack vektor, operational impact, defense, informational impact, target
---
## Pros
 - Gute Struktur
 - Tabellen und Figuren sind großteils brauchbar
 - Anwendbarkeit von AVOIDIT nachvollziehbar

Note:
 Figuren und Tabellen erleichtern Vergleiche geben guten Überblick über KLassen
 -- Beispiele anhand des Conficker wurm
---

## Cons
 - Alt
 - Methode zur Erstellung der Kategorien nicht beschrieben
 - Figur in "Future Work" unbrauchbar

Note:
 Taxonomien/Klassifizierungen müssen aktualisiert werden (changing technology) --
 Klassen scheinen von CVE/CWE abgeleitet zu sein (Namen oder Wortlaut ca. gleich)--
 Figur zeigt metasploit exploit execution (so what) --
---

## Fragen
