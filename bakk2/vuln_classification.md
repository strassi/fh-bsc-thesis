# Vulnerability/Attack classification

CAPEC (Common Attack Enumeration and classification) provides a view for classification by attack domain:

  + Social Engineering - (403)  
  + Supply Chain - (437)  
  + Communications - (512)  
  + Software - (513)  
  + Physical Security - (514)  
  + Hardware - (515)  
  
Not applicable domains:

  - Physical Security (514) - Describes Access Control Bypasses of facilities and physical theft  
  - Supply Chain (437) - differ from distribution and manufacturing processes  
  - Social Engineering (403) - needs human in the loop. Only the possibility of human interfaces can be tested  
  
This leaves us with the following applicable/partly applicable categories:

  + [Communications - (512)](http://capec.mitre.org/data/definitions/512.html)  
  + [Software - (513)](http://capec.mitre.org/data/definitions/513.html)   
  + [Hardware - (515)](http://capec.mitre.org/data/definitions/515.html)  

