# Bsc Meeting
#### Requirements and limitations of cyber security testbeds

---
## Bisher

  - Anforderungen
    - Guidelines, Standards, Methodologien
  - Limitierungen
    - Schwachstellen, Betriebssysteme, Featureunterstützung

---
## Wo gibt es Probleme?

 - Auswahl der Softwareprodukte
 - Feature-Vergleich sinnvoll?
 - Schwachstellen testen
 - Test-Cases erstellen
 - Beispiel Tabellenstyle

---
### Auswahl der Softwareprodukte

<table style="font-size:60%;">
<thead>
 <tr>
  <th>Name</th><th>Grund</th>
 </tr>
</thead>
<tbody>
 <tr>
  <td>VMware vSphere</td><td>Verbreitung, Voll-Virtualisierung</td>
 </tr>
 <tr>
  <td>XEN Project</td><td>Verbreitung, Voll-Virtualisierung</td>
 </tr>
 <tr>
  <td>KVM</td><td>wird von RedHat forciert, Voll-Virtualisierung</td>
 </tr>
 <tr>
  <td>Docker</td><td>Container-Virtualisierung, Container-Virtualisierung</td>
 </tr>
 <tr>
  <td>Thin App</td><td>?? Applikation-Virtualisierung </td>
 </tr>
 <tr>
  <td>Hyper-V</td><td>Voll-Virtualisierung/[COntainer-Virtualisierung](https://docs.microsoft.com/en-us/virtualization/windowscontainers/about/)</td>
 </tr>
</tbody>
</table>

Note:
  - Verschiedene Virtualisierunglösungen, da Performance Rolle spielt
  - Thin App sehr neu, evt. Lizenzprobleme
  - Hyper-V / Windows-Container noch interessant?
  - Versionen alt oder neu?

---

### Feature-Vergleich sinnvoll?

<table style="font-size:60%">
<thead>
 <tr>
  <th>Feature</th><th>Fragestellung</th>
 </tr>
</thead>
<tbody>
 <tr>
  <td>Nested Virtualization</td><td>Können VM-Escapes getestet werden?</td>
 </tr>
 <tr>
  <td>PCI passthrough</td><td>Ist das durchschleifen von Hardware möglich?</td>
 </tr>
 <tr>
  <td>GUI support for guests</td><td>Kann auf Gäste-GUI zugegriffen werden?</td>
 </tr>
 <tr>
  <td>Windows Support</td><td>Kann Windows virtualisiert werden?</td>
 </tr>
 <tr>
  <td>Linux Support</td><td>Kann Linux virtualisiert werden?</td>
 </tr>
</tbody>
</table>

Note:

 - Unterstützung für Erweiterungen/Anbindung von Hardware
 - Unterstützung für Human-in-the-Loop Angriffe
 - Unterstützung für Virtualisierungsangriffe
 - Generell gute Idee?

---

### Schwachstellen testen

<table style="font-size:60%">
<thead>
 <tr>
  <th>Schwachstellen</th><th>Fragestellung</th>
 </tr>
</thead>
<tbody>
 <tr>
  <td>Meltdown/Spectre</td><td>Sind virtualisierungsprodukte verwundbar?</td>
 </tr>
 <tr>
  <td>RowHammer</td><td>Funktioniert es auch in Virtuellen Umgebungen?</td>
 </tr>
 <tr>
  <td>ARP Flooding/Spoofing</td><td>Schwachstellen der onboard-Netzwerimpl. ?</td>
 </tr>
 <tr>
  <td>VM Detection</td><td>Ist es möglich festzustellen ob Gast virtualisiert ist?</td>
 </tr>
 <tr>
  <td>Info Gathering</td><td>Kann Hardware vorgetäuscht werden #malware-sandbox?</td>
 </tr>
</tbody>
</table>


Note:

 - Nur Schwachstellen testen zu wenig, da Kategorien/Attack Domain meist gleich
 - VMM Komponenten Memory, CPU, Networking, Storage angreifen

---
### Test-Cases erstellen

  - Ubuntu / Windows (Versionen 14.04/WIN7/WIN2008) als Opfer
  - Kali-Linux als Angreifer
  - Vorgehen nach Pentest Guidelines/Standards
  
Note:

 - Problem wegen der Vergleichbarkeit
 - Versionen alt -> Exploits funktionieren
 - Alles in virtualisierter Umgebung
 - Guideline/Standard Genauigkeit? - Info. Gathering etc.

---
### Beispiel Tabellenstyle 1

Produkt | Feature1 | Feature2 | Exploit1 
------- | -------- | -------- | -------- 
Produkt1  |  YES     | YES    | N/A      
Produkt2  |  YES     | YES    | NO       

Note:

 - Vorteil: alles auf einen Blick
 - Nachteil: kann unübersichtlich werden

---
### Beispiel Tabellenstyle 2

Produkt | Feature1 | Feature2 | Feature3
------- | -------- | -------- | --------
Produkt1  |  YES     | YES      | NO    
Produkt2  |  YES     | YES      | NO    

Produkt | Exploit1 | Exploit2 | Exploit3
------- | -------- | -------- | --------
Produkt1  |  YES     | YES      | N/A    
Produkt2  |  YES     | YES      | NO     

Note:

 - Vorteil: abgrenzung der tests/bessere segmentierung
 - Nachteil: nicht alles beieinander/vergleichbarkeit schlechter für leser